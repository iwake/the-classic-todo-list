import { createRouter, createWebHistory } from 'vue-router'

// classic pages imports
import TodoList from '@/pages/TodoList.vue'
import TaskPage from '@/pages/TaskPage.vue'

const routes = [
    {
        path: '/',
        name: 'TodoList',
        component: TodoList,
        props: true
    },
    {
        path: '/task/:id(\\d+)',
        name: 'Task',
        component: TaskPage,
        props: true
    }
]

const router = createRouter({ history: createWebHistory(), routes })
export default router
