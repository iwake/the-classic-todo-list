import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './storage'
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faSquare, faSquareCheck, faArrowLeft, faEdit } from "@fortawesome/free-solid-svg-icons";

library.add(faSquare, faSquareCheck, faArrowLeft, faEdit);

const vueApp = createApp(App).component("font-awesome-icon", FontAwesomeIcon).use(router).use(store).mount('#app')
