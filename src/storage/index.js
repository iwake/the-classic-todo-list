import { createStore } from 'vuex'

const getDefaultState = () => {
    return {
        todoList: [
            {
                id: 0,
                title: 'Mettre à jour mon CV',
                description: '',
                completed: true
            },
            {
                id: 1,
                title: "Diffuser mon CV sur les sites d'emploi",
                description: '',
                completed: true
            },
            {
                id: 2,
                title: "Décrocher un entretien",
                description: '',
                completed: true
            },
            {
                id: 3,
                title: "Impressionner les recruteurs",
                description: '',
                completed: false
            },
            {
                id: 4,
                title: "Signer un contrat de travail",
                description: '',
                completed: false
            }
        ]
    }
}

const state = getDefaultState()

const getters = {
    todoList: state => state.todoList,
    currentTask: (state) => (id) => {
        return state.todoList.find(task => task.id === id)
    }
}

const actions = {}
  
const mutations = {}

export default createStore({
    state,
    getters,
    actions,
    mutations
  })
